﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class SpinnerNumberButton : MonoBehaviour
{
    public NumberManager numberManager;
    public Button[] buttons;
    public GameObject buttonClicked;
    public Text number;

    void Start()
    {
        foreach (Button button in buttons)
        {
            button.onClick.AddListener(ClickNumber);
        }
    }

    public void ButtonClick(GameObject thisButton)
    {
        buttonClicked = thisButton;
    }

    public void NumberChosen(Text numberChosen)
    {
        number = numberChosen;
    }

    public void ClickNumber()
    {
        numberManager = GameObject.FindWithTag("NumberManager").GetComponent<NumberManager>();
        numberManager.SelectNumber( number);
    }
}
