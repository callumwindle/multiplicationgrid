﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class NumberManager : MonoBehaviour
{
    public int number;
    public Text stringNumber;
    public Text selectedNumber;
    public static GameObject button;
    public Text maxAmountText;
    public int maxAmount;
    public int maxAmountBounds;
    public int minAmountBounds;

    void Start()
    {
        maxAmount = 10;
    }

    public void MaxAmountChange(int maxChange)
    {
        maxAmount = maxChange;
    }

    // public void IncreaseMax()
    // {
    //     if(maxAmount < maxAmountBounds)
    //     {
    //         maxAmount++;
    //         maxAmountText.text = maxAmount.ToString();
    //     }
    // }

    // public void DecreaseMax()
    // {
    //     if(maxAmount > minAmountBounds)
    //     {
    //         maxAmount--;
    //         maxAmountText.text = maxAmount.ToString();
    //     }
    // }

    public void SelectNumber(Text numberText)
    {
        stringNumber = numberText;
        number = int.Parse(stringNumber.text);
        selectedNumber.text = number.ToString();
    }

    public void AddOne()
    {
        if(number < maxAmount)
        {
            number++;
            selectedNumber.text = number.ToString();
            stringNumber.text = number.ToString();
        }
    }

    public void MinusOne()
    {
        if(number > 0)
        {
            number--;
            selectedNumber.text = number.ToString();
            stringNumber.text = number.ToString();
        }
    }
}
