using UnityEngine.Events;

namespace TwinklStandardClasses
{
    [System.Serializable]
    public class BoolEvent : UnityEvent<bool>
    {
    }
}