﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class SpinnerManager : MonoBehaviour
{
    public List<SpinnerController> spinners = new List<SpinnerController>();
    public List<GameObject> spinnersGO = new List<GameObject>();
    public List<GameObject> arrows = new List<GameObject>();
    public GameObject firstSpinner;
    public GameObject firstArrow;
    public GameObject parent;
    public GameObject arrowParent;
    public int spinnerCount;
    public int maxSpinners;
    public int minSpinners;
    public GameObject[] spinnerButtons;
    public GameObject otherShape;
    public GameObject[] spinnerShape;
    public int spinnerType;
    public int spinnerTypeMax;
    public GameObject[] allNumbers;
    public NumberManager numberManager;
    public GameObject[] spinnerTypeActiveOverlay;
    public Animator[] characterAnims;
    public AnimationClip[] animationLength;
    int charAnimsIndex;
    bool animationIsPlaying;

    void Start()
    {
        Init();

        foreach(GameObject spinneroverlay in spinnerTypeActiveOverlay)
        {
            spinneroverlay.SetActive(false);
        }

        spinnerTypeActiveOverlay[0].SetActive(true);
    }

    void Update()
    {
        for(int i = 0; i < spinnerCount; i++)
        {
            spinnerButtons[i].SetActive(true);
        }
    }

    void Init()
    {
        spinnerCount = 1;
    }

    public void SpinAll()
    {
        if(spinnerCount >= 1)
        {
            TriggerAnimState();
            foreach (SpinnerController spinner in spinners)
            {
                spinner.Spin();
            }
        }
    }

    public void SpinIndividual(int spinnerNum)
    {
        if(spinnerNum <= spinnerCount -1 && spinnerCount > 0)
        {
            TriggerAnimState();
            spinners[spinnerNum].Spin();        
        } 
    }

    IEnumerator ResetAnimState(float animationClipLength)
    {
        yield return new WaitForSeconds(animationClipLength);
        characterAnims[charAnimsIndex].SetBool("Action", false);
        animationIsPlaying = false;
    }

    void TriggerAnimState()
    {
        if(!animationIsPlaying)
        {
            animationIsPlaying = true;
            charAnimsIndex = Random.Range(0, characterAnims.Length);
            characterAnims[charAnimsIndex].SetBool("Action", true);
            // 0.25f is removed otherwise the animation overflows.
            float wait = animationLength[charAnimsIndex].length - 0.25f;
            StartCoroutine(ResetAnimState(wait));
        }
    }

    public void AddSpinner()
    {
        if(spinnerCount < maxSpinners)
        {
            GameObject additionalSpinner = Instantiate(spinnerShape[spinnerType], transform.position, Quaternion.identity);
            additionalSpinner.transform.SetParent(parent.transform, true);

            GameObject additionalArrow = Instantiate(firstArrow, transform.position, Quaternion.identity);
            additionalArrow.transform.SetParent(arrowParent.transform, true);

            SpinnerController spinnerScript = additionalSpinner.GetComponent<SpinnerController>();
            spinnerScript.arrow = additionalArrow.GetComponent<Image>();

            spinnersGO.Add(additionalSpinner);
            spinners.Add(additionalSpinner.GetComponent<SpinnerController>());
            arrows.Add(additionalArrow);
            spinnerCount++;
        }
    }

    public void ChangeSpinnerShape()
    {
        GameObject additionalSpinner = Instantiate(spinnerShape[spinnerType], transform.position, Quaternion.identity);
        additionalSpinner.transform.SetParent(parent.transform, true);

        GameObject additionalArrow = Instantiate(firstArrow, transform.position, Quaternion.identity);
        additionalArrow.transform.SetParent(arrowParent.transform, true);

        SpinnerController spinnerScript = additionalSpinner.GetComponent<SpinnerController>();
        spinnerScript.arrow = additionalArrow.GetComponent<Image>();

        spinnersGO.Add(additionalSpinner);
        spinners.Add(additionalSpinner.GetComponent<SpinnerController>());
        arrows.Add(additionalArrow);
    }

    public void RemoveSpinner()
    {
        if(spinnerCount > minSpinners)
        {
            Destroy(spinnersGO[0]);
            Destroy(arrows[0]);
            arrows.Remove(arrows[0]);
            spinnersGO.Remove(spinnersGO[0]);
            spinners.Remove(spinners[0]);
            spinnerCount--;
        }
    }

    public void ChangeShape(int spinnerChange)
    {
        foreach(GameObject spinneroverlay in spinnerTypeActiveOverlay)
        {
            spinneroverlay.SetActive(false);
        }

        spinnerTypeActiveOverlay[spinnerChange].SetActive(true);
        
        spinnerType = spinnerChange;

        for(int i = 0; i < spinners.Count; i++)
        {
            Destroy(spinnersGO[0]);
            spinnersGO.Remove(spinnersGO[0]);
            spinners.Remove(spinners[0]);
            Destroy(arrows[0]);
            arrows.Remove(arrows[0]);
            ChangeSpinnerShape();
        }
    }

    public void RandomNumbers()
    {
        allNumbers = GameObject.FindGameObjectsWithTag("Number");
        for(int i = 0; i < allNumbers.Length; i++)
        {
            int rand = Random.Range(0, numberManager.maxAmount);
            allNumbers[i].GetComponent<Text>().text = rand.ToString();
        }
    }
}
