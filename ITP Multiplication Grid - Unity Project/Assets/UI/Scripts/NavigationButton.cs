using TwinklGameManager;
using UnityEngine;

namespace UI.Scripts
{
    
    
    public class NavigationButton: MonoBehaviour
    {
        public void SwitchScene(string _sceneName)
        {
            SceneController.Instance.SwitchScene(_sceneName);
        }

        public void AddScene(string _sceneName)
        {
            SceneController.Instance.AddScene(_sceneName);
        }
    }
}