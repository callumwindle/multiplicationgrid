using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextTestScript : MonoBehaviour
{
    public Text[] txtBoxes;
     
         void Start () 
         {
                
         }
             
         public void SetTextSize ()
         {
     
             int minSize = 1000;
     
             //Find smallest
             for (int i = 0; i < txtBoxes.Length; i++)
             {
                 Debug.Log(txtBoxes[i].gameObject.activeInHierarchy);
                if(txtBoxes[i].gameObject.activeInHierarchy == true)
                {
                    if (minSize > txtBoxes [i].cachedTextGenerator.fontSizeUsedForBestFit)
                        minSize = txtBoxes [i].cachedTextGenerator.fontSizeUsedForBestFit;
                }
             }
     
             //Set smallest
             for (int i = 0; i < txtBoxes.Length; i++)
             {
                 txtBoxes[i].resizeTextMaxSize = minSize;
                 Debug.Log(minSize);
             }
         }
}
