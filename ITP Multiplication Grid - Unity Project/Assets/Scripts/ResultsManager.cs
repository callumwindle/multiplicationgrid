﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResultsManager : MonoBehaviour
{
    public Text sideNumText, topNumText, sumNumText;
    public int sideNum, topNum, sumNum;
    private string sideTemp, topTemp;
    public GameObject sideIncTens, topIncTens, topIncHun;
    public List<GameObject> sideBut, topBut = new List<GameObject>();
    public bool topHund, sideTen;
    private int topMax, topMin, sideMax, sideMin;
    public MultiplicationController multiControl;
    public AdditionManager addMan;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void ResetCount(int x)
    {
        switch(x)
        {
            case 2:
                sideNumText.text = "0";
                topNumText.text = "00";
            break;

            case 3:
                sideNumText.text = "0";
                topNumText.text = "000";
            break;

            case 4:
                sideNumText.text = "00";
                topNumText.text = "00";
            break;

            case 6:
                sideNumText.text = "00";
                topNumText.text = "000";
            break;
        }

        sideNum = 0;
        topNum = 0;
        sumNum = 0;
        sumNumText.text = "?";
    }
    
    public void EditSideCount(int x)
    {
        sumNumText.text = "?";
        sideTen = sideIncTens.activeSelf;
        if(sideTen)
        {
            sideMax = 99;
        }
        else
        {
            sideMax = 9;
        }

        switch(x)
        {
            case 1:
                sideNum++;
                if(sideNum > sideMax)
                {
                    sideNum = 0;
                }
            break;

            case -1:
                sideNum--;
                if(sideNum < 0)
                {
                    sideNum = sideMax;
                }
            break;

            case 10:
                sideNum += 10;
                if(sideNum > sideMax)
                {
                    sideNum -= (sideMax + 1);
                }
            break;

            case -10:
                sideNum -= 10;
                if(sideNum < 0)
                {
                    sideNum += (sideMax + 1);
                }
            break;
        }

        if(sideTen && sideNum < 10)
        {
            sideTemp = "0"+ sideNum.ToString();
        }
        else
        {
            sideTemp = sideNum.ToString();
        }

        sideNumText.text = sideTemp;

        multiControl.SeperateSide();
        multiControl.ResetSolutions();
        addMan.ResetWritten();
    }

    public void EditTopCount(int x)
    {
        sumNumText.text = "?";
        
        topHund = topIncHun.activeSelf;
        if(topHund)
        {
            topMax = 999;
        }
        else
        {
            topMax = 99;
        }
        
        switch(x)
        {
            case 1:
                topNum++;
                if(topNum > topMax)
                {
                    topNum = 0;
                }
            break;

            case -1:
                topNum--;
                if(topNum < 0)
                {
                    topNum = (topMax);
                }
            break;

            case 10:
                topNum += 10;
                if(topNum > topMax)
                {
                    topNum -= (topMax+1);
                }
            break;
            
            case -10:
                topNum -= 10;
                if(topNum < 0 )
                {
                    topNum += (topMax+1);
                }
            break;

            case 100:
                topNum += 100;
                if(topNum > topMax)
                {
                    topNum -= (topMax+1);
                }
            break;

            case -100:
                topNum -= 100;
                if(topNum < 0)
                {
                    topNum += (topMax+1);
                }
            break;
        }
        
        if(topNum < 10)
        {
            topTemp = "0" + topNum.ToString();
        }
        else
        {
            topTemp = topNum.ToString();
        }
        if(topHund && topNum < 100)
        {
            topTemp = "0" + topTemp;
        }        
        
        topNumText.text = topTemp;
        multiControl.SeperateTop();
        multiControl.ResetSolutions();
        addMan.ResetWritten();
    }

    public void ChangeNumbers(int x)
    {
        switch(x)
        {
            case 2:
                foreach(GameObject i in sideBut)
                {
                    i.SetActive(false);
                }
                sideNumText.text = "0";
                foreach(GameObject i in topBut)
                {
                    i.SetActive(false);
                }
                topNumText.text = "00";
            break;

            case 3:
                foreach(GameObject i in topBut)
                {
                    i.SetActive(true);
                }
                topNumText.text = "000";
            break;

            case 4:
                foreach(GameObject i in topBut)
                {
                    i.SetActive(false);
                }
                topNumText.text = "00";
                foreach(GameObject i in sideBut)
                {
                    i.SetActive(true);
                }
                sideNumText.text = "00";
            break;

            case 6:
                foreach(GameObject i in topBut)
                {
                    i.SetActive(true);
                }
                topNumText.text = "000";
            break;
        }
        sideNum = 0;
        topNum = 0;
        sumNum = 0;
    }

    public void SolutionButton()
    {
        if(sumNumText.text == "?")
        {
            sumNum = sideNum*topNum;
            sumNumText.text = sumNum.ToString();
            addMan.ShowSolution(sumNum);
        }
        else
        {
            sumNumText.text = "?";
            addMan.HideSolution();
        }
    }
}
