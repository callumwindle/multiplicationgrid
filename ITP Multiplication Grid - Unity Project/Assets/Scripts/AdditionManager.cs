﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AdditionManager : MonoBehaviour
{
    public List<GameObject> additionTexts, placeHolders = new List<GameObject>();
    public Text resultText;
    
    // Start is called before the first frame update
    void Start()
    {
        foreach(GameObject i in additionTexts)
        {
            i.GetComponent<Text>().text = "";
        }

        resultText.text = "?";
    }

    public void ShowResult(int result, int block)
    {
        additionTexts[(block-1)].GetComponent<Text>().text = result.ToString();
    }

    public void HideResult(int block)
    {
        additionTexts[(block-1)].GetComponent<Text>().text = "";
    }

    public void ShowSolution(int x)
    {
        resultText.text = x.ToString();
    }
    public void HideSolution()
    {
        resultText.text = "?";
    }

    public void ResetWritten()
    {
        foreach(GameObject i in additionTexts)
        {
            i.GetComponent<Text>().text = "";
        }

        resultText.text = "?";
    }

    public void EditSize(int gridSize)
    {
        switch(gridSize)
        {    
            case 2:
                foreach(GameObject i in additionTexts)
                {
                    i.SetActive(false);
                }
                additionTexts[1].SetActive(true);
                additionTexts[2].SetActive(true);
                foreach(GameObject i in placeHolders)
                {
                    i.SetActive(true);
                }
            break;

            case 3:
                additionTexts[0].SetActive(true);
                placeHolders[0].SetActive(false);
            break;

            case 4:
                additionTexts[0].SetActive(false);
                placeHolders[1].SetActive(false);
                additionTexts[4].SetActive(true);
                additionTexts[5].SetActive(true);

            break;

            case 6:
                foreach(GameObject i in additionTexts)
                {
                    i.SetActive(true);
                }
                foreach(GameObject i in placeHolders)
                {
                    i.SetActive(false);
                }
            break;
        }
    }
}
