﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MultiplicationController : MonoBehaviour
{
    public List<GameObject> topResult, botResult, topNum, sidNum = new List<GameObject>();
    public int sidMult, topMult, resNum, gridSize, sidCount, topCount;
    public string resStr, sidStr, topStr;
    public bool sideIsTens;
    public GameObject placeHolder;
    public int i, currGridSize, sideTens, sideOnes, topHuns, topTens, topOnes;
    public ResultsManager resMan;
    public AdditionManager addMan;
    public TextTestScript textMan;
    
    // Start is called before the first frame update
    void Start()
    {
        ResetResults();
        gridSize = 2;
        ChangeSize();
    }   

    public void ResetMultipliers()
    {   
        foreach(GameObject i in topNum)
        {
            i.GetComponent<Text>().text = "0";
        }

        foreach(GameObject i in sidNum)
        {
            i.GetComponent<Text>().text = "0";
        }

        sideOnes = 0;
        sideTens = 0;
        topHuns = 0;
        topTens = 0; 
        topOnes = 0;
    }

    public void EditMultipliers(int x)
    {
        switch(x)
        {
            case 2:
                sidNum[0].GetComponent<Text>().text = sideOnes.ToString();
                topNum[1].GetComponent<Text>().text = topTens.ToString();
                topNum[2].GetComponent<Text>().text = topOnes.ToString();
            break;

            case 3:
                sidNum[0].GetComponent<Text>().text = sideOnes.ToString();
                topNum[0].GetComponent<Text>().text = topHuns.ToString();
                topNum[1].GetComponent<Text>().text = topTens.ToString();
                topNum[2].GetComponent<Text>().text = topOnes.ToString();
            break;

            case 4:
                sidNum[0].GetComponent<Text>().text = sideTens.ToString();
                sidNum[1].GetComponent<Text>().text = sideOnes.ToString();
                topNum[1].GetComponent<Text>().text = topTens.ToString();
                topNum[2].GetComponent<Text>().text = topOnes.ToString();
            break;

            case 6:
                sidNum[0].GetComponent<Text>().text = sideTens.ToString();
                sidNum[1].GetComponent<Text>().text = sideOnes.ToString();
                topNum[0].GetComponent<Text>().text = topHuns.ToString();
                topNum[1].GetComponent<Text>().text = topTens.ToString();
                topNum[2].GetComponent<Text>().text = topOnes.ToString();
            break;
        }
    }

    public void ResetResults()
    {
        ResetSolutions();
        resNum = 0;
        ResetMultipliers();
        resMan.ResetCount(currGridSize);
        addMan.ResetWritten();
    }

    public void ResetSolutions()
    {
        foreach(GameObject i in topResult)
        {
            i.transform.GetChild(0).GetComponent<Text>().text = "?";
        }

        foreach(GameObject i in botResult)
        {
            i.transform.GetChild(0).GetComponent<Text>().text = "?";
        }
    }

    public void SolveMe(int x, int y)
    {
        resNum = 0;

        sidMult = Int32.Parse(sidNum[y].GetComponent<Text>().text);
        topMult = Int32.Parse(topNum[x].GetComponent<Text>().text);
            
        resNum = sidMult*topMult;     
    }

    public void SolveResult(int buttonPressed)
    { 
        switch(buttonPressed)
        {
            case 1:
                if(topResult[0].transform.GetChild(0).GetComponent<Text>().text == "?")
                {
                    SolveMe(0,0);
                    topResult[0].transform.GetChild(0).GetComponent<Text>().text = resNum.ToString();
                    addMan.ShowResult(resNum, buttonPressed);
                }
                else
                {
                    topResult[0].transform.GetChild(0).GetComponent<Text>().text = "?";
                    addMan.HideResult(buttonPressed);
                }
            break;

            case 2:
                if(topResult[1].transform.GetChild(0).GetComponent<Text>().text == "?")
                {
                    SolveMe(1,0);
                    topResult[1].transform.GetChild(0).GetComponent<Text>().text = resNum.ToString();
                    addMan.ShowResult(resNum, buttonPressed);
                }
                else
                {
                    topResult[1].transform.GetChild(0).GetComponent<Text>().text = "?";
                    addMan.HideResult(buttonPressed);
                }
            break;

            case 3:
                if(topResult[2].transform.GetChild(0).GetComponent<Text>().text == "?")
                {
                    SolveMe(2,0);
                    topResult[2].transform.GetChild(0).GetComponent<Text>().text = resNum.ToString();
                    addMan.ShowResult(resNum, buttonPressed);
                }
                else
                {
                    topResult[2].transform.GetChild(0).GetComponent<Text>().text = "?";
                    addMan.HideResult(buttonPressed);
                }
            break;

            case 4:
                if(botResult[0].transform.GetChild(0).GetComponent<Text>().text == "?")
                {
                    SolveMe(0,1);
                    botResult[0].transform.GetChild(0).GetComponent<Text>().text = resNum.ToString();
                    addMan.ShowResult(resNum, buttonPressed);
                }
                else
                {
                    botResult[0].transform.GetChild(0).GetComponent<Text>().text = "?";
                    addMan.HideResult(buttonPressed);
                }
            break;

            case 5:
                if(botResult[1].transform.GetChild(0).GetComponent<Text>().text == "?")
                {
                    SolveMe(1,1);
                    botResult[1].transform.GetChild(0).GetComponent<Text>().text = resNum.ToString();
                    addMan.ShowResult(resNum, buttonPressed);
                }
                else
                {
                    botResult[1].transform.GetChild(0).GetComponent<Text>().text = "?";
                    addMan.HideResult(buttonPressed);
                }
            break;

            case 6:
                if(botResult[2].transform.GetChild(0).GetComponent<Text>().text == "?")
                {
                    SolveMe(2,1);
                    botResult[2].transform.GetChild(0).GetComponent<Text>().text = resNum.ToString();
                    addMan.ShowResult(resNum, buttonPressed);
                }
                else
                {
                    botResult[2].transform.GetChild(0).GetComponent<Text>().text = "?";
                    addMan.HideResult(buttonPressed);
                }
            break;
        }
        textMan.SetTextSize();
    }

    public void ChangeSize()
    {
        ResetMultipliers();
        ResetResults();
        currGridSize = gridSize;

        switch(gridSize)
        {
            case 2:
                sideIsTens = true;
                topNum[0].SetActive(false);
                sidNum[1].SetActive(false);
                foreach(GameObject i in botResult)
                {
                    i.SetActive(false);
                }
                topResult[0].SetActive(false);
                placeHolder.SetActive(true);
                resMan.ChangeNumbers(gridSize);
                resMan.ResetCount(gridSize);
                
                gridSize++;
            break;
            
            case 3:
                sideIsTens = true;
                topNum[0].SetActive(true);
                sidNum[1].SetActive(false);
                foreach(GameObject i in botResult)
                {
                    i.SetActive(false);
                }
                topResult[0].SetActive(true);
                placeHolder.SetActive(true);
                resMan.ChangeNumbers(gridSize);
                resMan.ResetCount(gridSize);
                gridSize++;
            break;

            case 4:
                sideIsTens = false;
                placeHolder.SetActive(false);
                topNum[0].SetActive(false);
                sidNum[1].SetActive(true);
                for(i=1; i<3; i++)
                {
                    botResult[i].SetActive(true);
                }
                topResult[0].SetActive(false);
                botResult[0].SetActive(false);
                resMan.ChangeNumbers(gridSize);
                resMan.ResetCount(gridSize);
                gridSize += 2;
            break;

            case 6:
                sideIsTens = false;
                topNum[0].SetActive(true);
                botResult[0].SetActive(true);
                topResult[0].SetActive(true);
                resMan.ChangeNumbers(gridSize);
                resMan.ResetCount(gridSize);
                gridSize = 2;
            break;  
        }
        addMan.EditSize(currGridSize);

    }
    
    public void SeperateTop()
    {
        topStr = resMan.topNumText.text;

        if(resMan.topHund)
        {
            topHuns = Int32.Parse(topStr[0].ToString())*100;
            topTens = Int32.Parse(topStr[1].ToString())*10;
            topOnes = Int32.Parse(topStr[2].ToString());
        }
        else
        {
            topTens = Int32.Parse(topStr[0].ToString())*10;
            topOnes = Int32.Parse(topStr[1].ToString());
        }

        EditMultipliers(currGridSize);
    }

    public void SeperateSide()
    {
        sidStr = resMan.sideNumText.text;

        if(resMan.sideTen)
        {
            sideTens = Int32.Parse(sidStr[0].ToString())*10;
            sideOnes = Int32.Parse(sidStr[1].ToString()); 
        }
        else
        {
            sideOnes = Int32.Parse(sidStr[0].ToString());
        }
        
        EditMultipliers(currGridSize);
    }
}
